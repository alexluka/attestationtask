package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.ui.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.data.network.AppRepository
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.databinding.FragmentMainBinding
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.di.appComponent
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.ui.recyclerview.FormAdapter
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class MainFragment : Fragment() {

    private lateinit var mainViewModel: MainViewModel
    private lateinit var binding: FragmentMainBinding
    private val adapter: FormAdapter = FormAdapter()

    @Inject
    fun initViewModel(appRepository: AppRepository) {
        mainViewModel = MainViewModel(appRepository)
        mainViewModel.items.observe(this, Observer {
            if (it != null) {
                Snackbar.make(
                    binding.constraint,
                    "Форма загружена",
                    Snackbar.LENGTH_LONG).show()
                binding.apply {
                    Glide.with(this@MainFragment).load(it.image).into(imageView)
                    toolbar.title = it.title
                    adapter.submitList(it.fields)
                    formList.adapter = adapter
                }
            } else{
                Snackbar.make(
                    binding.constraint,
                    "Ошибка загрузки",
                    Snackbar.LENGTH_LONG).show()
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentMainBinding.inflate(inflater).also {
        binding = it
        binding.formList.layoutManager = LinearLayoutManager(activity)
        binding.formList.adapter = FormAdapter()
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.button.setOnClickListener {
            Toast.makeText(activity, adapter.getResults().toString(), Toast.LENGTH_LONG).show()
        }
    }

    override fun onAttach(context: Context) {
        context.appComponent.inject(this)
        super.onAttach(context)
    }
}