package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.ui.recyclerview

import android.R
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.databinding.FormItemBinding
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model.ResultData

class FormViewHolder(
    private val binding: FormItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    private var result = ""

    fun onBind(item: ResultData) {
        binding.textView.text = item.title
        when (item.type) {
            "TEXT" -> {
                val editText = EditText(binding.formItem.context)
                editText.layoutParams = LinearLayout.LayoutParams(
                    500,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                binding.formItem.addView(editText)
                result = ""
            }
            "NUMERIC" -> {
                val editText = EditText(binding.formItem.context)
                editText.inputType =
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                editText.layoutParams = LinearLayout.LayoutParams(
                    500,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                binding.formItem.addView(editText)
                result = ""
            }
            "LIST" -> {
                val spinner = Spinner(binding.formItem.context)
                spinner.layoutParams = LinearLayout.LayoutParams(
                    500,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                binding.formItem.addView(spinner)
                val variantsArray =
                    arrayOf(item.values?.none, item.values?.v1, item.values?.v2, item.values?.v3)
                val arrayAdapter = ArrayAdapter(binding.formItem.context, R.layout.simple_spinner_item, variantsArray)
                spinner.adapter = arrayAdapter
                result = variantsArray[0].toString()
                spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        result = variantsArray[position].toString()
                    }
                    override fun onNothingSelected(parent: AdapterView<*>) {
                        result = variantsArray[0].toString()
                    }
                }
            }
        }
    }

    fun getResult() : String = result

    companion object {
        fun create(parent: ViewGroup) = FormItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ).let(::FormViewHolder)
    }
}
