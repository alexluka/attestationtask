package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.R
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.ui.fragment.MainFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, MainFragment())
                .commit()
        }
    }
}