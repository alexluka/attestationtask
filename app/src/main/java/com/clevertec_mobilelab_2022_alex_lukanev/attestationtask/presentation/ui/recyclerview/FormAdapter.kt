package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.ui.recyclerview

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model.ResultData

class FormAdapter :
    ListAdapter<ResultData, FormViewHolder>(FormDiffCallBack()) {
    private var results = mutableListOf<String>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FormViewHolder =
        FormViewHolder.create(parent)

    override fun onBindViewHolder(holder: FormViewHolder, position: Int) {
        holder.onBind(getItem(position))
        results.add(holder.getResult())
    }

    fun getResults() : MutableList<String> {
        results
        return results
    }
}