package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.di

import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.data.network.AppRepository
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.data.network.AppRepositoryImpl
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.data.network.AppService
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

private const val BASE_URL = "http://test.clevertec.ru"
@Module(includes = [AppBindModule::class])
class AppModule {

    @Provides
    fun getAppService(retrofit: Retrofit): AppService {
        return retrofit.create(AppService::class.java)
    }

    @Provides
    fun getRetrofitInstance(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())//GsonConverterFactory.create()
            .build()
    }
}

@Module
interface AppBindModule {

    @Suppress("FunctionName")
    @Binds
    fun bindAppRepositoryImpl_to_AppRepository(
        newsRepositoryImpl: AppRepositoryImpl
    ): AppRepository
}