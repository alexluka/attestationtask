package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.ui.recyclerview

import androidx.recyclerview.widget.DiffUtil
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model.ResultData

class FormDiffCallBack : DiffUtil.ItemCallback<ResultData>() {
    override fun areItemsTheSame(oldItem: ResultData, newItem: ResultData): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: ResultData, newItem: ResultData): Boolean =
        oldItem == newItem
}