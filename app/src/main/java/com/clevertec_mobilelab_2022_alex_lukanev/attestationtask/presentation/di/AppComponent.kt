package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.di

import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.ui.activity.MainActivity
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.ui.fragment.MainFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(fragment: MainFragment)
}