package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.data.network.AppRepository
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model.Data
import kotlinx.coroutines.launch

class MainViewModel(
    private val appRepository: AppRepository
) : ViewModel() {

    private val _items = MutableLiveData<Data?>()
    val items: LiveData<Data?> get() = _items

    init {
        viewModelScope.launch {
            _items.value = appRepository.getDataForm()
        }
    }
}