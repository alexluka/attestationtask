package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.data.network

import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model.Data
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.usecase.DataFormUseCase
import javax.inject.Inject

class AppRepositoryImpl @Inject constructor(
    private val dataFormUseCase: DataFormUseCase
) : AppRepository {
    override suspend fun getDataForm(): Data? {
        return dataFormUseCase.getDataForm()
    }
}