package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.data.network

import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model.ApiData
import retrofit2.Response
import retrofit2.http.GET

interface AppService {
    @GET("/tt/meta/")
    suspend fun getForm(): Response<ApiData>
}