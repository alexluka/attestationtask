package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.data.network

import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model.Data

interface AppRepository {
    suspend fun getDataForm(): Data?
}