package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model

data class Data(
    val title: String?,
    val image: String?,
    val fields: List<ResultData?>
)

data class ResultData(
    val id: Int?,
    val title: String?,
    val name: String?,
    val type: String?,
    val values: ValuesData?
)

data class ValuesData(
    val none: String?,
    val v1: String?,
    val v2: String?,
    val v3: String?,
)