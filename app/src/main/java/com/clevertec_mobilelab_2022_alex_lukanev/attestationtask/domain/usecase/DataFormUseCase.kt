package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.usecase

import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.data.network.AppService
import com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DataFormUseCase @Inject constructor(private val appService: AppService) {
    suspend fun getDataForm(): Data? {
        val items: ApiData? = appService.getForm().body()
        var data: Data? = null
        val fields = mutableListOf<ResultData>()
        withContext(Dispatchers.IO) {
            if (items != null) {
                var i = 0
                items.fields.forEach { item ->
                    if (item?.values != null) {
                        val values = ValuesData(
                            item.values.none,
                            item.values.v1,
                            item.values.v2,
                            item.values.v3
                        )
                        val textData = ResultData(i++, item.title, item.name, item.type, values)
                        fields.add(textData)
                    } else {
                        val textData = ResultData(i++, item?.title, item?.name, item?.type, null)
                        fields.add(textData)
                    }
                }
                data = Data(items.title, items.image, fields)
            }
        }
        return data
    }
}