package com.clevertec_mobilelab_2022_alex_lukanev.attestationtask.domain.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiData(
    @Json(name = "title") val title: String?,
    @Json(name = "image") val image: String?,
    @Json(name = "fields") val fields: List<Result?>
)

@JsonClass(generateAdapter = true)
data class Result(
    @Json(name = "title") val title: String?,
    @Json(name = "name") val name: String?,
    @Json(name = "type") val type: String?,
    @Json(name = "values") val values: ValuesResult?
)

@JsonClass(generateAdapter = true)
data class ValuesResult(
    @Json(name = "none") val none: String?,
    @Json(name = "v1") val v1: String?,
    @Json(name = "v2") val v2: String?,
    @Json(name = "v3") val v3: String?
)